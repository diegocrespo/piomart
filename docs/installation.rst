piomart is on pip. This is the most recommended way to install it.

.. code-block:: bash

  pip install piomart

Manual installation
+++++++++++++++++++

.. code-block:: bash

  git clone git@gitlab.com:diegocrespo/piomart.git
  

.. code-block:: bash

  python setup.py install

the required packages for this software are


* python packages (python>=3.5)
    - docopt>=0.6.2
    - setuptools>=30.0
    - numpy>=1.14.3
    - responses>=0.9.0
    - pandas>=0.23.0
    - requests>=2.18.4