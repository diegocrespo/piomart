FROM python:3.6

COPY requirements.txt requirements.txt

RUN pip install --trusted-host pypi.python.org -r requirements.txt

# mount the current working directory to /cwd when the container is run
WORKDIR /tests

ADD tests/test_piomart.py
# Run app.py when the container launches
CMD ["python", "test_piomart.py"]
